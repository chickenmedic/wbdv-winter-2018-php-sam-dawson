@extends('layout')

@section('content')

<?php if ($message = session('message')): ?>
    <div class="alert alert-success">
        <?php echo $message ?>
    </div>
<?php endif; ?>

<?php if($errors->any()):  ?>
    <div class="alert alert-danger">
        <ul>
            <?php foreach ($errors->all() as $error): ?>
                <li><?php echo $error ?></li>
            <?php endforeach; ?>
        </ul>
    </div>
<?php endif ?>

<div class="container">

    <h1>Contact Us</h1>

    <form method="post">
        <?php echo csrf_field() ?>

        @include('forms.text', [
            'label' => 'Name',
            'name' => 'name'
        ])

        @include('forms.text', [
            'label' => 'Email',
            'name' => 'email'
        ])

        <label for="message">Message:</label>
        <textarea name="message" class="form-control <?php echo $errors->has('message') ? 'is-invalid' : '' ?>" rows="8" cols="80"  placeholder="Your comment, query, or concern"><?php echo old('message') ?></textarea>
        <br>
        <input type="submit" name="" value="Submit" class="btn my-4 sub-btn">


    </form>
</div>





@endsection
