
@extends('layout')

@section('content')
<div class="container">
    <h1>Articles Feed</h1>
    <a href="/contact" class="btn my-4 sub-btn">Contact</a>
    <a href="/article" class="btn my-4 sub-btn">Write</a>
    <a href="/article/{id}" class="btn my-4 sub-btn">Your Feed</a>
    <h2>Users</h2>
    <ul>
        <?php foreach($users as $user): ?>
            <li>
                <?php echo $user->name ?>
                <?php echo $user->username ?>

                This user likes  <?php echo count($user->likes) ?> articles.
                <ul>
                    <?php foreach ($user->articles as $article): ?>
                        <li><?php echo $article->content ?></li>
                        Date: <?php echo $article->updated_at->format('Y-m-d') ?>
                        <li>
                            This article liked by <?php echo count($article->likes) ?> users.
                        </li>
                    <?php endforeach; ?>
                </ul>
            </li>
        <?php endforeach; ?>
    </ul>

<h2>Articles</h2>
    <ul>
        <?php foreach($articles as $article): ?>
            <li>
                @include("partials.article")
            </li>
        <?php endforeach; ?>
    </ul>

</div>
@endsection
