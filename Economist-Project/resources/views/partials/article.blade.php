@extends('layout')

@section('content')

Username: {{ $article->user->username }}
<br>
{{$article->content}}

@if ($article->isLikedByCurrentUser())
    <a href="/articles/{{$article->id}}/like/toggle">Unike</a>
@else
    <a href="/articles/{{$article->id}}/like/toggle">Like</a>
@endif

<ul>
    @foreach ($article->comments as $comment)
        <li>
            Comment:
            <br>
            {{$comment->content}}
            <br>
            User: {{$comment->user->username}}
            <br>
            Date: {{$comment->updated_at->diffForHumans()}}
        </li>
    @endforeach
</ul>
@endsection
