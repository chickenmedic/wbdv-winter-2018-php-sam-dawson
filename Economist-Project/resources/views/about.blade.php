<li class="nav-item btn my-2 my-sm-0 sub-btn">

        <a class="btn my-2 my-sm-0 sub-btn" href="{{ route('logout') }}"
           onclick="event.preventDefault();
                         document.getElementById('logout-form').submit();">
            Logout
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>

</li>
