<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>The Economist - World News, Politics, Economics, Business & Finance</title>
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div id="app">
        <div class="navbar-color sticky-top">

            <div class="economist-container nav-container">
                <nav class="navbar navbar-expand-md navbar-laravel">
                    <a class="navbar-brand" href="/">
                        <div class="economist-logo">
                            <span>The</span>
                            <span>Economist</span>
                        </div>
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Topics
                                </a>
                                <div class="dropdown-menu topics" aria-labelledby="navbarDropdownMenuLink">
                                    <div class="topics-dropdown">
                                        <div class="all-topics">
                                            <h4 class="topic-head col-md-8">Sections</h4>
                                            <h4 class="topic-head col-md-4">Blogs</h4>
                                        </div>
                                        <div class="all-topics">
                                            <div class="topic-columns-2">
                                                <div class="col-md-12">
                                                    <a class="dropdown-item menu" href="/articles">Articles</a>
                                                    <a class="dropdown-item" href="/article">Write</a>
                                                    <a class="dropdown-item" href="/contact">Contact Us</a>
                                                    <a class="dropdown-item" href="/comment">Comment</a>
                                                    <a class="dropdown-item" href="#">United States</a>
                                                    <a class="dropdown-item" href="#">The Americas</a>
                                                    <a class="dropdown-item" href="#">Asia</a>
                                                    <a class="dropdown-item" href="#">China</a>
                                                    <a class="dropdown-item" href="#">Middle East and Africa</a>
                                                    <a class="dropdown-item" href="#">Europe</a>
                                                    <a class="dropdown-item special" href="#">Britain</a>
                                                </div>
                                                <div class="col-md-12">
                                                    <a class="dropdown-item" href="#">International</a>
                                                    <a class="dropdown-item" href="#">Business</a>
                                                    <a class="dropdown-item" href="#">Finance and Economics</a>
                                                    <a class="dropdown-item" href="#">Science and Technology</a>
                                                    <a class="dropdown-item" href="#">Books and Art</a>
                                                    <a class="dropdown-item" href="#">Obituary</a>
                                                    <a class="dropdown-item" href="#">Special Reports</a>
                                                    <a class="dropdown-item" href="#">Technology Quarterly</a>
                                                    <a class="dropdown-item" href="#">Debates</a>
                                                </div>
                                            </div>
                                            <div class="topic-columns-1">
                                                <div class="col-md-12">
                                                    <a class="dropdown-item" href="#">Bagehot's notebook</a>
                                                    <a class="dropdown-item" href="#">Buttonwood's notebook</a>
                                                    <a class="dropdown-item" href="#">Democracy in America</a>
                                                    <a class="dropdown-item" href="#">Erasmus</a>
                                                    <a class="dropdown-item" href="#">Free Exchange</a>
                                                    <a class="dropdown-item" href="#">Game theory</a>
                                                    <a class="dropdown-item" href="#">Graphic detail</a>
                                                    <a class="dropdown-item" href="#">Gulliver</a>
                                                    <a class="dropdown-item" href="#">Kaffeeklatsch</a>
                                                    <a class="dropdown-item" href="#">Prospero</a>
                                                    <a class="dropdown-item" href="#">The Economist explains</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">Current Edition</a>
                            </li>
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    More
                                </a>
                                <div class="dropdown-menu more" aria-labelledby="navbarDropdownMenuLink">
                                    <div class="topics-dropdown">
                                        <div class="all-topics">
                                            <h4 class="topic-head col-md-4">Apps & Digital Editions</h4>
                                            <h4 class="topic-head col-md-4">From the Economist Group</h4>
                                            <h4 class="topic-head col-md-4">Media</h4>
                                        </div>
                                        <div class="all-topics">
                                            <div class="topic-columns-1">
                                                <div class="col-md-12">
                                                    <a class="dropdown-item" href="#">The Economist apps</a>
                                                    <a class="dropdown-item" href="#">Espresso</a>
                                                    <a class="dropdown-item" href="#">Global Business Review</a>
                                                    <a class="dropdown-item" href="#">World in Figures</a>

                                                    <h4 class="topic-head col-md-12 special">Other Publications</h4>
                                                    <a class="dropdown-item" href="#">1843 Magazine</a>
                                                    <a class="dropdown-item" href="#">The World In</a>
                                                    <a class="dropdown-item special-padding" href="#">The World If</a>
                                                </div>
                                            </div>
                                            <div class="topic-columns-1">
                                                <div class="col-md-12">
                                                    <a class="dropdown-item" href="#">Events</a>
                                                    <a class="dropdown-item" href="#">Online GMAT prep</a>
                                                    <a class="dropdown-item" href="#">Online GRE prep</a>
                                                    <a class="dropdown-item" href="#">Executive Education Navigator</a>
                                                    <a class="dropdown-item" href="#">Which MBA</a>
                                                    <a class="dropdown-item" href="#">Jobs Board</a>
                                                    <a class="dropdown-item" href="#">Learning.ly</a>
                                                    <a class="dropdown-item" href="#">The Economist Store</a>
                                                    <a class="dropdown-item" href="#">The Economist Intelligence Unit</a>
                                                    <a class="dropdown-item" href="#">The Economist Corporate Network</a>
                                                </div>
                                            </div>
                                            <div class="topic-columns-1">
                                                <div class="col-md-12">
                                                    <a class="dropdown-item" href="#">Audio Edition</a>
                                                    <a class="dropdown-item" href="#">Economist Film</a>
                                                    <a class="dropdown-item" href="#">Economist Radio</a>
                                                    <h4 class="special"><em>About the Economist</em></h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <form class="form-inline">
                        <a href="/articles" class="btn my-2 my-sm-0 sub-btn">Articles</a>
                        <a href="/register" class="btn my-2 my-sm-0 sub-btn">Register</a>
                        @if (Auth::check())
                        <li class="nav-item btn my-2 my-sm-0 sub-btn">

                                <a class="btn my-2 my-sm-0 sub-btn" href="/home">
                                    Logout
                                </a>

                        </li>
                        @else
                            <a href="/login" class="btn my-2 my-sm-0 sub-btn">Login</a>
                        @endif
                    </form>
                </nav>
            </div>
        </div>

        @yield('content')
        </div>
    <script src="{{ asset('/js/app.js') }}"></script>
    </body>
</html>
