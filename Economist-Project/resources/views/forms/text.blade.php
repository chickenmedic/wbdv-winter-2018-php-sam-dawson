<div class="form-group">
    <label for="{{ $name }}">
        {{$label}}:
    </label>
    <input
        type="text"
        name="{{ $name }}"
        placeholder="{{ $label }}"
        class="form-control {{ $errors->has($name) ? 'is-invalid' : '' }}"
        value="{{ old($name) }}"/>

    @if($errors->has($name))
        <span class="invalid-feedback">
            {{ $errors->first($name) }}
        </span>
    @endif
    <br>
</div>
