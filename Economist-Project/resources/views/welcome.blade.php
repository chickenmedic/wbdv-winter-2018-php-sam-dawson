@extends('layout')

@section('content')

<div class="economist-container">
    <div class="topper">
        <div class="main-content">
            <div class="hero-wrapper">
                <div class="hero-primary">
                    <a href="#">
                        <div class="info-primary">
                            <h5>World Trade</h5>
                            <h4>
                                <?php foreach($usere as $user): ?>
                                    <?php echo $user->test ?>
                                <?php endforeach; ?>
                            </h4>
                            <p>
                                <em>
                                    <?php foreach($useri as $user): ?>
                                        <?php echo $user->test ?>
                                    <?php endforeach; ?>
                                </em>
                            </p>
                        </div>
                        <span>Finance & Economics</span>
                    </a>
                    <div class="photo-primary">
                        <?php foreach($usera as $user): ?>
                            <img src="<?php echo $user->image ?>"/>
                        <?php endforeach; ?>
                    </div>
                </div>
                <div>
                    <hr>
                </div>
                <div class="hero-secondary">
                    <div class="sizer">
                        <a href="#">
                        <div class="photo-secondary">
                            <?php foreach($userb as $user): ?>
                                <img src="<?php echo $user->image ?>"/>
                            <?php endforeach; ?>
                        </div>
                            <div class="info-secondary">
                                <h5>After the fall</h5>
                                <h4>
                                    <?php foreach($userf as $user): ?>
                                        <?php echo $user->test ?>
                                    <?php endforeach; ?>
                                </h4>
                                <p>Economics</p>
                            </div>
                        </a>
                    </div>
                        <div class="seperator"></div>
                    <div class="sizer">
                        <a href="#">
                        <div class="photo-secondary">
                            <?php foreach($userc as $user): ?>
                                <img src="<?php echo $user->image ?>"/>
                            <?php endforeach; ?>
                        </div>
                            <div class="info-secondary">
                                <h5>Merkel does things</h5>
                                <h4>
                                    <?php foreach($userg as $user): ?>
                                        <?php echo $user->test ?>
                                    <?php endforeach; ?>
                                </h4>
                                <p>Business</p>
                            </div>
                        </a>
                    </div>
                        <div class="seperator"></div>
                    <div class="sizer">
                        <a href="#">
                        <div class="photo-secondary">
                            <?php foreach($userd as $user): ?>
                                <img src="<?php echo $user->image ?>"/>
                            <?php endforeach; ?>
                        </div>
                            <div class="info-secondary">
                                <h5>Guns are bad</h5>
                                <h4>
                                    <?php foreach($userh as $user): ?>
                                        <?php echo $user->test ?>
                                    <?php endforeach; ?>
                                </h4>
                                <p>America</p>
                            </div>
                        </a>
                    </div>

                </div>

            </div>
            <div class="article-sidebar">
                <div class="sidebar-top">
                    <h5>Latest Stories</h5>
                </div>
                <div class="sidebar-content">
                    <div class="sidebar-wrapper">
                        <a href="#" class="sidebar-main">
                                <?php foreach($userh as $user): ?>
                                    <?php echo $user->test ?>
                                <?php endforeach; ?>
                        </a>
                        <br>
                        <a href="#" class="sidebar-small">
                            Erasmus
                        </a>
                        <hr>
                    </div>
                    <div class="sidebar-wrapper">
                        <a href="#" class="sidebar-main">
                                <?php foreach($usere as $user): ?>
                                    <?php echo $user->test ?>
                                <?php endforeach; ?>
                        </a>
                        <br>
                        <a href="#" class="sidebar-small">
                            Who d'un it
                        </a>
                        <hr>
                    </div>
                    <div class="sidebar-wrapper">
                        <a href="#" class="sidebar-main">
                                <?php foreach($userf as $user): ?>
                                    <?php echo $user->test ?>
                                <?php endforeach; ?>
                        </a>
                        <br>
                        <a href="#" class="sidebar-small">
                            Game Theory
                        </a>
                        <hr>
                    </div>
                    <div class="sidebar-wrapper">
                        <a href="#" class="sidebar-main">
                                <?php foreach($userg as $user): ?>
                                    <?php echo $user->test ?>
                                <?php endforeach; ?>
                        </a>
                        <br>
                        <a href="#" class="sidebar-small">
                            The theory of everything
                        </a>
                        <hr>
                    </div>
                    <div class="sidebar-wrapper">
                        <a href="#" class="sidebar-main">
                                <?php foreach($userh as $user): ?>
                                    <?php echo $user->test ?>
                                <?php endforeach; ?>
                        </a>
                        <br>
                        <a href="#" class="sidebar-small">
                            Pickles are the best
                        </a>
                        <hr>
                    </div>
                    <div class="sidebar-wrapper">
                        <a href="#" class="sidebar-main">
                                <?php foreach($useri as $user): ?>
                                    <?php echo $user->test ?>
                                <?php endforeach; ?>
                        </a>
                        <br>
                        <a href="#" class="sidebar-small">
                            Other things
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="latest-news-mediaQuery">
    <div class="container">
        <div class="latest-stories">
            <h4>Latest Stories</h4>
        </div>
    </div>
    <div class="container">
        <div class="stories">
            <div class="content">
                <h4>
                    <?php foreach($userh as $user): ?>
                        <?php echo $user->test ?>
                    <?php endforeach; ?>
                </h4>
                <h6>Something</h6>
            </div>
            <hr>
            <div class="content">
                <h4>
                    <?php foreach($usere as $user): ?>
                        <?php echo $user->test ?>
                    <?php endforeach; ?>
                </h4>
                <h6>Something</h6>
            </div>
            <hr>
            <div class="content">
                <h4>
                    <?php foreach($userf as $user): ?>
                        <?php echo $user->test ?>
                    <?php endforeach; ?>
                </h4>
                <h6>Something</h6>
            </div>
            <hr>
            <div class="content">
                <h4>
                    <?php foreach($useri as $user): ?>
                        <?php echo $user->test ?>
                    <?php endforeach; ?>
                </h4>
                <h6>Something</h6>
            </div>
            <hr>
            <div class="content">
                <h4>
                    <?php foreach($userh as $user): ?>
                        <?php echo $user->test ?>
                    <?php endforeach; ?>
                </h4>
                <h6>Something</h6>
            </div>
            <hr>
            <div class="content">
                <h4>
                    <?php foreach($userf as $user): ?>
                        <?php echo $user->test ?>
                    <?php endforeach; ?>
                </h4>
                <h6>Something</h6>
            </div>
        </div>
    </div>
</div>

<div class="economist-container">
    <div class="main-split">
        <div class="current-content">
            <div class="current-edition">
                <div class="edition-frontpage">
                    <div class="edition">
                        <img src="https://www.economist.com/printedition/2018-03-03/cover-image/1600" alt="">
                    </div>
                </div>
                <div class="edition-content">
                    <div class="content-info">
                        <div class="info">
                            <h3>Current Edition</h3>
                            <p>Mar 3<sup>rd</sup> 2018</p>
                        </div>
                        <div class="current-edition-links">
                            <div class="links-left">
                                <a class="edition-content" href="#">

                                        <span class="section">Geopolitics</span>
                                        <span class="title">
                                            <?php foreach($usere as $user): ?>
                                                <?php echo $user->test ?>
                                            <?php endforeach; ?>
                                        </span>

                                </a>
                                <a class="edition-content" href="#">
                                    <p>
                                        <span class="section">The environment</span>
                                        <span class="title">
                                            <?php foreach($userf as $user): ?>
                                                <?php echo $user->test ?>
                                            <?php endforeach; ?>
                                        </span>
                                    </p>
                                </a>
                                <a class="edition-content" href="#">
                                    <p>
                                        <span class="section">Self-driving cars</span>
                                        <span class="title">
                                            <?php foreach($userg as $user): ?>
                                                <?php echo $user->test ?>
                                            <?php endforeach; ?>
                                        </span>
                                    </p>
                                </a>
                                <a class="edition-content no-border" href="#">
                                    <p>
                                        <span class="section">Security</span>
                                        <span class="title">
                                            <?php foreach($userh as $user): ?>
                                                <?php echo $user->test ?>
                                            <?php endforeach; ?>
                                        </span>
                                    </p>
                                </a>

                                <div class="edition-button">
                                    <a href="#" class="no-border">See full edition</a>
                                </div>
                            </div>
                            <div class="links-right">
                                <a class="edition-content" href="#">
                                    <p>
                                        <span class="section">America</span>
                                        <span class="title">
                                            <?php foreach($useri as $user): ?>
                                                <?php echo $user->test ?>
                                            <?php endforeach; ?>
                                        </span>
                                    </p>
                                </a>
                                <a class="edition-content" href="#">
                                    <p>
                                        <span class="section">United States</span>
                                        <span class="title">
                                            <?php foreach($usere as $user): ?>
                                                <?php echo $user->test ?>
                                            <?php endforeach; ?>
                                        </span>
                                    </p>
                                </a>
                                <a class="edition-content" href="#">
                                    <p>
                                        <span class="section">Somewhere in the world</span>
                                        <span class="title">
                                            <?php foreach($userf as $user): ?>
                                                <?php echo $user->test ?>
                                            <?php endforeach; ?>
                                        </span>
                                    </p>
                                </a>
                                <a class="edition-content no-border" href="#">
                                    <p>
                                        <span class="section">Another headline</span>
                                        <span class="title">
                                            <?php foreach($userg as $user): ?>
                                                <?php echo $user->test ?>
                                            <?php endforeach; ?>
                                        </span>
                                    </p>
                                </a>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="sidebar-empty">
                <!-- to keep the right side empty -->
            </div>
        </div>
    </div>
</div>


<div class="economist-container">
    <div class="main-split">
        <div class="current-content">
            <div class="featured-articles">
                <div class="featurette-wrapper">
                    <div class="featurette">
                        <div class="image">
                            <?php foreach($userl as $user): ?>
                                <img src="<?php echo $user->image ?>"/>
                            <?php endforeach; ?>
                        </div>
                        <div class="text">
                            <p>The environment</p>
                            <h3>
                                <?php foreach($useri as $user): ?>
                                    <?php echo $user->test ?>
                                <?php endforeach; ?>
                            </h3>
                        </div>
                    </div>
                    <div class="featurette">
                        <div class="image">
                            <?php foreach($userm as $user): ?>
                                <img src="<?php echo $user->image ?>"/>
                            <?php endforeach; ?>
                        </div>
                        <div class="text">
                            <p>Britian something</p>
                            <h3>
                                <?php foreach($useri as $user): ?>
                                    <?php echo $user->test ?>
                                <?php endforeach; ?>
                            </h3>
                        </div>
                    </div>
                    <div class="featurette">
                        <div class="image">
                            <?php foreach($userk as $user): ?>
                                <img src="<?php echo $user->image ?>"/>
                            <?php endforeach; ?>
                        </div>
                        <div class="text">
                            <p>Canada did something, amazing</p>
                            <h3>
                                <?php foreach($useri as $user): ?>
                                    <?php echo $user->test ?>
                                <?php endforeach; ?>
                            </h3>
                        </div>
                    </div>
                    <div class="featurette">
                        <div class="image">
                            <?php foreach($userm as $user): ?>
                                <img src="<?php echo $user->image ?>"/>
                            <?php endforeach; ?>
                        </div>
                        <div class="text">
                            <p>Trump is crazy, probably</p>
                            <h3>
                                <?php foreach($useri as $user): ?>
                                    <?php echo $user->test ?>
                                <?php endforeach; ?>
                            </h3>
                        </div>
                    </div>
                    <div class="featurette">
                        <div class="image">
                            <?php foreach($userj as $user): ?>
                                <img src="<?php echo $user->image ?>"/>
                            <?php endforeach; ?>
                        </div>
                        <div class="text">
                            <p>I'm out of ideas</p>
                            <h3>
                                <?php foreach($useri as $user): ?>
                                    <?php echo $user->test ?>
                                <?php endforeach; ?>
                            </h3>
                        </div>
                    </div>
                </div>
                <div class="featurette-wrapper featurette-right">
                <div class="featurette featurette-right">
                    <div class="image">
                        <?php foreach($userj as $user): ?>
                            <img src="<?php echo $user->image ?>"/>
                        <?php endforeach; ?>
                    </div>
                    <div class="text">
                        <p>How to finance</p>
                        <h3>
                            <?php foreach($useri as $user): ?>
                                <?php echo $user->test ?>
                            <?php endforeach; ?>
                        </h3>
                    </div>
                </div>
                <div class="featurette featurette-right">
                    <div class="image">
                        <?php foreach($userk as $user): ?>
                            <img src="<?php echo $user->image ?>"/>
                        <?php endforeach; ?>
                    </div>
                    <div class="text">
                        <p>China's nice this time of year</p>
                        <h3>
                            <?php foreach($useri as $user): ?>
                                <?php echo $user->test ?>
                            <?php endforeach; ?>
                        </h3>
                    </div>
                </div>
                <div class="featurette featurette-right">
                    <div class="image">
                        <?php foreach($userl as $user): ?>
                            <img src="<?php echo $user->image ?>"/>
                        <?php endforeach; ?>
                    </div>
                    <div class="text">
                        <p>France is protesting for some reason</p>
                        <h3>
                            <?php foreach($useri as $user): ?>
                                <?php echo $user->test ?>
                            <?php endforeach; ?>
                        </h3>
                    </div>
                </div>
                <div class="featurette featurette-right">
                    <div class="image">
                        <?php foreach($userm as $user): ?>
                            <img src="<?php echo $user->image ?>"/>
                        <?php endforeach; ?>
                    </div>
                    <div class="text">
                        <p>Incredible story</p>
                        <h3>
                            <?php foreach($useri as $user): ?>
                                <?php echo $user->test ?>
                            <?php endforeach; ?>
                        </h3>
                    </div>
                </div>
                <div class="featurette featurette-right">
                    <div class="image">
                        <?php foreach($userj as $user): ?>
                            <img src="<?php echo $user->image ?>"/>
                        <?php endforeach; ?>
                    </div>
                    <div class="text">
                        <p>Not clickbait</p>
                        <h3>
                            <?php foreach($useri as $user): ?>
                                <?php echo $user->test ?>
                            <?php endforeach; ?>
                        </h3>
                    </div>
                </div>
            </div>
            </div>
            <div class="sidebar-empty">
                <!-- to keep the right side empty -->
            </div>
        </div>
    </div>
</div>
@endsection
