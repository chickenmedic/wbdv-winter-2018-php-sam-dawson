@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
                <div class="inline">


                    <form class="form-inline">
                        <a href="/register" class="btn my-2 my-sm-0 sub-btn">Register</a>
                        <a href="/contact" class="btn my-2 my-sm-0 sub-btn">Contact</a>
                        <a href="/" class="btn my-2 my-sm-0 sub-btn">Home</a>
                        @if (Auth::check())
                        <li class="nav-item btn my-2 my-sm-0 sub-btn">

                                <a class="btn my-2 my-sm-0 sub-btn" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                    Logout
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>

                        </li>
                        @else
                            <a href="/login">Login</a>
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
