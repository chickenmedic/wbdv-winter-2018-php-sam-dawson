@extends('layout')

@section('content')

<div class="form-control">
    <form method="post">
        <?php echo csrf_field() ?>

        @include('forms.text', [
        'label' => 'Content',
        'name' => 'content'
        ])

        <input type="submit" name="" value="Submit"class="btn my-2 my-sm-0 sub-btn">
    </form>
</div>

@endsection
