@extends('layout')

@section('content')

<div class="economist-container">
    <?php echo csrf_field() ?>
    <h1>{{ $user->name }}</h1>

    <div class="card-body">
        @foreach ($articles as $article)
        <li>{{$article->content}}</li>
        @endforeach
    </div>
</div>





@endsection
