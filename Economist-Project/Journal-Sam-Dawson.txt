PHP Journal
Sam Dawson
Tuesday February 27, 2018 � Tuesday March 13, 2018

1. After beginning to learn PHP with the help of the Sqola app I decided 
to go back and find some further practice that I could work with. I decided
 to try out code academy and played with some of their modules on PHP.
 I found the object oriented programming parts 1 and 2 to be helpful in wrapping
 my head around the idea of what object oriented programming is. I would also note
 that I found the sqola app very helpful in getting to grips with PHP. 

2.  Over the weekend (March 3rd and 4th) I worked on our first assignment and 
found that it would be helpful to learn more bootstrap as I found it really helped 
me speed up the design of my assignment. It was especially helpful in spacing things
out and creating the navigation. Although I definitely hit snags when I wanted to 
change the look and feel of the navigation and needed to spend some quality time on 
stack overflow to understand how to get things looking and working the way I wanted. 
I got much more familiar with stack overflow and how to search through their information
which I think will come in handy for the final assignment. 

3. After starting to learn about php I decided that it would be beneficial to convert my
portfolio into a Laravel website and to work on deploying it to an EC2 instance. This 
ran me into many road blocks that got quite irritating if I�m honest. Mostly the issue 
I kept running into was that the ssh key wasn�t working quite right. Looking back on it 
I think I wasn�t putting the ssh key into bitbucket correctly. I also had issues with 
getting the virtual host to work on my computer at home. The issue there that I kept 
running into was that xampp couldn�t find the file I was looking for which I think was 
down to not giving the files the correct permissions and thus my computer was blocking 
xampp from looking at files that I had placed outside of the htdocs folder. Again I used 
stack overflow to help figure out what was going wrong and did eventually get everything 
ship shape. Although it was a little frustrating to sit down for three hours and only work 
on getting the dev environment setup instead of actually putting that time into building my 
portfolio website. The benefit is that I�ve got lots of practice setting up and tearing down 
EC2 instances and getting Laravel to startup. 

4. On Wednesday March 7, I decided that I needed to go back through the videos casts that 
were made in class to help cement the ideas in my mind. I felt that it was especially helpful 
to watch the video and take notes and then to practice doing the same thing again and if I got 
stuck I could refer back to the video. This definitely helped me to get a better feel for how 
Laravel is structured and where everything is located. I continued to watch several of the videos 
and make notes when I felt that I hadn�t quite grasped what was done in class. If I�m honest it 
would�ve been nice if you had slowed down a little and went step by step, at times I felt you were 
blitzing through the content. Although equally it was really nice to have a bunch of time to practice 
actually doing what you had just shown. 

5. Over the last weekend of the course I decided that it would be nice to have a logo for my site. 
I especially wanted some sort of Favicon for my personal website as I felt that it would look a 
little more professional then having that blank document looking thing there instead. To do this 
I used what we had learned in our photoshop and illustrator course to make something that I was 
quite happy with. It was really useful looking to places like Pinterest to get ideas on how to make 
a good logo. I also found looking at tutorials on YouTube really helped to give me some things to 
think about when creating a logo, such as which colours to use and how to keep it simple. I then 
exported the file and put it onto my website and got it working as my favicon. This again lead me 
to read up on how to do this in Laravel on Stack overflow, and of course it was as simple as swapping 
the file named favicon with the image that I had created. In any event I�m really proud of the way it 
turned out. 
