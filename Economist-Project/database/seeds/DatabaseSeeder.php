<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use App\Models\Article;
use App\Models\Comment;
use App\User;


class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Factory::create();

        $user = new User();
        $user->name = $faker->name;
        $user->username = $faker->word;
        $user->image = $faker->imageUrl(100,100, 'people', true, false);
        $user->email = $faker->unique()->email;
        $user->password = $faker->password;
        $user->save();

        for($i=0; $i<rand(1,10); $i++){
            $article = new Article();
            $article->content = $faker->text(900);
            $article->user_id = $user->id;
            $article->save();

            $article->likes()->attach($user);

            for ($j=0; $j < rand(1,10); $j++) {
                $comment = new Comment();
                $comment->content = $faker->text(200);
                $comment->user_id = $user->id;
                $comment->article_id = $article->id;
                $comment->save();
            }
        }

    }
}
