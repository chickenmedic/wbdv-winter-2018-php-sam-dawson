<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Faker\Factory;
use App\User;

Route::get('/about', 'AboutController@index');

Route::get('/contact', 'ContactController@create');
Route::post('/contact', 'ContactController@store');

Route::get('/comment', 'ContactController@create');
Route::post('/comment', 'ContactController@store');

Route::get('/article', 'ArticleController@create');
Route::post('/article', 'ArticleController@store');
Route::get('/articles/{id}/like/toggle', 'ArticleController@toggleLike');

Route::get('/articles/{id}', 'ArticleController@userArticles');

Route::get('/articles', 'MainController@index');

// this stuff just makes the front page work with faker data
Route::get('/', function () {

    $faker = Factory::create();

    $user1 = new User();
    $user2 = new User();
    $user3 = new User();
    $user4 = new User();
    $user5 = new User();
    $user6 = new User();
    $user7 = new User();
    $user8 = new User();
    $user9 = new User();
    $user10 = new User();
    $user11 = new User();
    $user12 = new User();
    $user13 = new User();

    $user1->image = $faker->imageURL($width = 640, $height = 350, 'city');
    $user2->image = $faker->imageURL($width = 300, $height = 170, 'technics');
    $user3->image = $faker->imageURL($width = 300, $height = 170, 'fashion');
    $user4->image = $faker->imageURL($width = 300, $height = 170, 'business');
    $user5->test = $faker->bs;
    $user6->test = $faker->bs;
    $user7->test = $faker->bs;
    $user8->test = $faker->bs;
    $user9->test = $faker->bs;
    $user10->image = $faker->imageURL($width = 230, $height = 130, 'city');
    $user11->image = $faker->imageURL($width = 230, $height = 130, 'technics');
    $user12->image = $faker->imageURL($width = 230, $height = 130, 'city');
    $user13->image = $faker->imageURL($width = 230, $height = 130, 'technics');

    $data = [
        'usera'=> [$user1],
        'userb'=> [$user2],
        'userc'=> [$user3],
        'userd'=> [$user4],
        'usere'=> [$user5],
        'userf'=> [$user6],
        'userg'=> [$user7],
        'userh'=> [$user8],
        'useri'=> [$user9],
        'userj'=> [$user10],
        'userk'=> [$user11],
        'userl'=> [$user12],
        'userm'=> [$user13],
    ];
    return view('welcome', $data);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
