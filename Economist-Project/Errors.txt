1.Create a laravel app

I should be able to log in and out using Laravel's built-in auth
it should be deployed on an EC2 with a virtual host.

	-I deployed this to my website economist.samdawson.ca 

2.When logged in...
	I should be able to read content of all users on the homepage:

		-I didn�t feel that this would make sense for the Economist so I�ve moved user content to the �articles� page

	I should be able to add content as the logged-in user

		-I was unable to get this functionality working. I had it working on the localhost, though it wouldn�t let me logout on the localhost. I�d say that that was a feature. Tried to fix it by routing users to the register or home page where the logout button did work. A little weird for user functionality, but it sorta worked. 
	
	Content must be validated using a 'required' validator, and at least one other validator. I will try to break your forms by submitting them empty, and with various kinds of crazy input.
		
		-Unfortunately the forms are sort of broken already. When I migrated the project to my website it seemed to stop working for the login function, when the error log is turned on nothing seemed to come up so I�m not too sure why this is happening. 
	
	Content should have links to the content author, so I can see that user's content

		-Couldn�t get this functionality working. But using articles/{username} will take you to their page. This only works by manually typing it in. 
	
	I should be able to view a list of all content

		-It looks like the loop that should make this happen has stopped working. I�m sure there�s a simple fix but I�m unable to figure out the problem 
	
	I should be able to view the list of a single user's content

		-This sort of works, but I can�t get it to work through a button. I�m sure you showed us in the video somewhere, but I don�t have time to go through and watch it again. You are able to go to specific users pages by manually typing in their username or user_id
	
	I should be able to add a comment to any piece of 'core' content

		-Couldn�t get this working 

	I should be able to 'like' and 'unlike' any piece of content by clicking on an appropriate icon

		-This worked on the local setup, but stopped working when I moved it to the EC2. Couldn�t figure out why this was happening. 

	When logged out...
	I should be able to view a list of all content
		
		-This works but you can only see one persons content, I believe the loop isn�t working properly but I don�t know why it stopped working. 
	
	I should not be able to create content, comments, or like content

		-You can�t even login, so technically I guess this is true. 

	I should be able to fill out a contact form

		-This does actually work. 

	All pages should be accessible by links. i.e. I don't want to have to look at your web.php to know what your routes are.

		-This is about half true. There are some things that aren�t working though. 

3.Setup...

	I should be able to run php artisan migrate:fresh to set up a compatible database. (i.e. your database definition should be in your migrations)

		-I do believe that this works. 
