<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;

class ContactController extends Controller
{
    public function create() {


       return view('contact');
   }

    public function store() {

        $request = request();

        $result = $request->validate([
            'name' => 'required|max:255',
            'email' => 'required|max:255',
            'message' => 'required'
        ]);

       $data = request()->all();
       $contact = new Contact();
       $contact->name = $data['name'];
       $contact->email = $data['email'];
       $contact->message = $data['message'];
       $contact->save();

       return redirect('/contact')->with('message', 'Thank you for your comment, query,
                                                or concern. Someone will be in contact with you as soon as possible.
                                               (1-3 Business days)');
   }
}
