<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\Article;

class MainController extends Controller
{
    public function index() {
        $users = User::all();
        $articles = Article::all();

        $data = [
            'users' => $users,
            'articles' => $articles
        ];
        return view('articles', $data);
    }
}
